/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */

/* $Id$ */

/* Copyright (C) 1998-99 Martin Baulig
   This file is part of LibGTop 1.0.

   Contributed by Martin Baulig <martin@home-of-linux.org>, April 1998.

   LibGTop is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

   LibGTop is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with LibGTop; see the file COPYING. If not, write to the
   Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.
*/

#include <glibtop.h>
#include <glibtop/global.h>

#include <glibtop/union.h>
#include <glibtop/call-vector.h>

#include <glibtop/open.h>
#include <glibtop/xmalloc.h>
#include <glibtop/glibtop-backend.h>
#include <glibtop/glibtop-backend-info.h>

#include <glibtop-server-private.h>

static int
_open_sysdeps (glibtop_server *server, glibtop_backend *backend,
	       u_int64_t features, const char **backend_args)
{
    glibtop_init_func_t *init_fkt;
    glibtop_closure *machine;

    machine = glibtop_calloc_r (server, 1, sizeof (glibtop_closure));

    glibtop_backend_set_closure_data (backend, machine);

    glibtop_open_s (server, machine, features, 0);

    for (init_fkt = _glibtop_init_hook_s; *init_fkt; init_fkt++)
	(*init_fkt) (server, machine);

    return 0;
}

static int
_close_sysdeps (glibtop_server *server, glibtop_backend *backend,
		glibtop_closure *closure)
{
    glibtop_free_r (server, closure);

    return 0;
}

static glibtop_call_vector _glibtop_call_vector = {
    glibtop_get_cpu_s,
    glibtop_get_mem_s,
    glibtop_get_swap_s,
    glibtop_get_uptime_s,
    glibtop_get_loadavg_s,
    glibtop_get_shm_limits_s,
    glibtop_get_msg_limits_s,
    glibtop_get_sem_limits_s,
    glibtop_get_proclist_s,
    glibtop_get_proc_state_s,
    glibtop_get_proc_uid_s,
    glibtop_get_proc_mem_s,
    glibtop_get_proc_time_s,
    glibtop_get_proc_signal_s,
    glibtop_get_proc_kernel_s,
    glibtop_get_proc_segment_s,
    glibtop_get_proc_cwd_s,
    glibtop_get_proc_args_s,
    glibtop_get_proc_map_s,
    NULL,
    NULL,
    glibtop_get_interface_names_s,
    glibtop_get_netinfo_s,
    glibtop_get_netload_s,
    glibtop_get_ppp_s
};

static glibtop_init_func_t _glibtop_init_hook_s [] = {
    glibtop_init_cpu_s,
    glibtop_init_mem_s,
    glibtop_init_swap_s,
    glibtop_init_uptime_s,
    glibtop_init_loadavg_s,
    glibtop_init_shm_limits_s,
    glibtop_init_msg_limits_s,
    glibtop_init_sem_limits_s,
    glibtop_init_proclist_s,
    glibtop_init_proc_state_s,
    glibtop_init_proc_uid_s,
    glibtop_init_proc_mem_s,
    glibtop_init_proc_time_s,
    glibtop_init_proc_signal_s,
    glibtop_init_proc_kernel_s,
    glibtop_init_proc_segment_s,
    glibtop_init_proc_cwd_s,
    glibtop_init_proc_args_s,
    glibtop_init_proc_map_s,
    glibtop_init_interface_names_s,
    glibtop_init_netinfo_s,
    glibtop_init_netload_s,
    glibtop_init_ppp_s,
    NULL
};

glibtop_backend_info LibGTopBackendInfo = {
    "glibtop-backend-sysdeps-stub", _open_sysdeps, _close_sysdeps,
    &_glibtop_call_vector
};
