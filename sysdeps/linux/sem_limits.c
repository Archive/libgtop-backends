/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */

/* $Id$ */

/* Copyright (C) 1998-99 Martin Baulig
   This file is part of LibGTop 1.0.

   Contributed by Martin Baulig <martin@home-of-linux.org>, April 1998.

   LibGTop is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

   LibGTop is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with LibGTop; see the file COPYING. If not, write to the
   Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.
*/

#include <glibtop/sem_limits.h>

#include <sys/ipc.h>
#include <sys/sem.h>

#ifdef _SEM_SEMUN_UNDEFINED

/* glibc 2.1 will no longer defines semun, instead it defines
 * _SEM_SEMUN_UNDEFINED so users can define semun on their own.
 * Thanks to Albert K T Hui <avatar@deva.net>. */

union semun
{
    int val;
    struct semid_ds *buf;
    unsigned short int *array;
    struct seminfo *__buf;
};
#endif

#include <glibtop-server-private.h>

static unsigned long _glibtop_sysdeps_sem_limits =
(1L << GLIBTOP_SEM_LIMITS_SEMMAP) + (1L << GLIBTOP_SEM_LIMITS_SEMMNI) +
(1L << GLIBTOP_SEM_LIMITS_SEMMNS) + (1L << GLIBTOP_SEM_LIMITS_SEMMNU) +
(1L << GLIBTOP_SEM_LIMITS_SEMMSL) + (1L << GLIBTOP_SEM_LIMITS_SEMOPM) +
(1L << GLIBTOP_SEM_LIMITS_SEMUME) + (1L << GLIBTOP_SEM_LIMITS_SEMUSZ) +
(1L << GLIBTOP_SEM_LIMITS_SEMVMX) + (1L << GLIBTOP_SEM_LIMITS_SEMAEM);

/* Init function. */

int
glibtop_init_sem_limits_s (glibtop_server *server, glibtop_closure *machine)
{
    server->info->sysdeps.sem_limits = _glibtop_sysdeps_sem_limits;

    return 0;
}

/* Provides information about sysv ipc limits. */

int
glibtop_get_sem_limits_s (glibtop_server *server, glibtop_closure *machine, glibtop_sem_limits *buf)
{
    struct seminfo	seminfo;
    union semun	arg;  
  
    memset (buf, 0, sizeof (glibtop_sem_limits));
  
    buf->flags = _glibtop_sysdeps_sem_limits;
  
    arg.array = (ushort *) &seminfo;
    semctl (0, 0, IPC_INFO, arg);
  
    buf->semmap = seminfo.semmap;
    buf->semmni = seminfo.semmni;
    buf->semmns = seminfo.semmns;
    buf->semmnu = seminfo.semmnu;
    buf->semmsl = seminfo.semmsl;
    buf->semopm = seminfo.semopm;
    buf->semume = seminfo.semume;
    buf->semusz = seminfo.semusz;
    buf->semvmx = seminfo.semvmx;
    buf->semaem = seminfo.semaem;

    return 0;
}
