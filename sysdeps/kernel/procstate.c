/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */

/* $Id$ */

/* Copyright (C) 1998-99 Martin Baulig
   This file is part of LibGTop 1.0.

   Contributed by Martin Baulig <martin@home-of-linux.org>, April 1998.

   LibGTop is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

   LibGTop is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with LibGTop; see the file COPYING. If not, write to the
   Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.
*/

#include <glibtop.h>
#include <glibtop/procstate.h>

#include <glibtop_private.h>

static const unsigned long _glibtop_sysdeps_proc_state =
(1L << GLIBTOP_PROC_STATE_UID) + (1L << GLIBTOP_PROC_STATE_GID) +
(1L << GLIBTOP_PROC_STATE_RUID) + (1L << GLIBTOP_PROC_STATE_RGID) +
(1L << GLIBTOP_PROC_STATE_CMD) + (1L << GLIBTOP_PROC_STATE_STATE) +
(1L << GLIBTOP_PROC_STATE_HAS_CPU) + (1L << GLIBTOP_PROC_STATE_PROCESSOR) +
(1L << GLIBTOP_PROC_STATE_LAST_PROCESSOR);

/* Init function. */

int
glibtop_init_proc_state_k (glibtop *server)
{
    server->sysdeps.proc_state = _glibtop_sysdeps_proc_state;

    return 0;
}

/* Provides detailed information about a process. */

int
glibtop_get_proc_state_k (glibtop *server, glibtop_proc_state *buf,
			  pid_t pid)
{
    libgtop_proc_state_t proc_state;
    int retval;

    memset (buf, 0, sizeof (glibtop_proc_state));

    retval = glibtop_get_proc_data_proc_state_k (server, &proc_state, pid);
    if (retval)
	return retval;

    memcpy (buf->cmd, proc_state.comm, sizeof (buf->cmd));

    if (proc_state.state & LIBGTOP_TASK_RUNNING)
	buf->state |= GLIBTOP_PROCESS_RUNNING;
    if (proc_state.state & LIBGTOP_TASK_INTERRUPTIBLE)
	buf->state |= GLIBTOP_PROCESS_INTERRUPTIBLE;
    if (proc_state.state & LIBGTOP_TASK_UNINTERRUPTIBLE)
	buf->state |= GLIBTOP_PROCESS_UNINTERRUPTIBLE;
    if (proc_state.state & LIBGTOP_TASK_ZOMBIE)
	buf->state |= GLIBTOP_PROCESS_ZOMBIE;
    if (proc_state.state & LIBGTOP_TASK_STOPPED)
	buf->state |= GLIBTOP_PROCESS_STOPPED;
    if (proc_state.state & LIBGTOP_TASK_SWAPPING)
	buf->state |= GLIBTOP_PROCESS_SWAPPING;

    buf->uid = proc_state.euid;
    buf->gid = proc_state.egid;
    buf->ruid = proc_state.uid;
    buf->rgid = proc_state.gid;

    buf->has_cpu = proc_state.has_cpu;
    buf->processor = proc_state.processor;
    buf->last_processor = proc_state.last_processor;

    buf->flags = _glibtop_sysdeps_proc_state;

    return 0;
}
